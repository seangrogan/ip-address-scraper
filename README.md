# README #

The attached python code scrapes the current ip address from [here](https://api.ipify.org/?format=json) to create a text document that had ip information on my driopbox so I could access a computer that had a non-static ip address.  

You will need to create a [windows task to execute the script automatically](https://blogs.esri.com/esri/arcgis/2013/07/30/scheduling-a-scrip/).  I did it twice a day, once at 5:00am and again at 12:00pm noon, local time.  

## LINKS ##

* https://api.ipify.org/?format=json
* https://blogs.esri.com/esri/arcgis/2013/07/30/scheduling-a-scrip/